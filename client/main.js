Messages = new Mongo.Collection('messages');
if (Meteor.isClient) {
    // counter starts at 0
    Session.setDefault('counter', 0);

    Template.hello.helpers({
        messages: function () {
            return Messages.find();
        }
    });

    Template.hello.events({
        'submit .chat-form': function (evt) {
            evt.preventDefault();
            var text = evt.target.message.value;
            console.log(text);
            Meteor.call('insertMessage', text, function(err, result) {
                if(err) {
                    console.log(err);
                } else {
                    console.log('Message inserted with ID: ', result);
                    evt.target.message.value = '';
                }
            });
        }
    });
}

if (Meteor.isServer) {
    Meteor.methods({
        insertMessage: function (text) {
            return Messages.insert({
                text: text,
                timestamp: Date.now()
            });
        }
    });
    
    Meteor.publish('messages', function(limit) {
        return Messages.find({}, {
            limit: limit || 5,
            sort: { timestamp: -1}
        })
    });
}